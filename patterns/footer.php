<?php
 /**
  * Title:Footer
  * Slug: woocommerce-fse/footer
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"full","backgroundColor":"background-secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull has-background-secondary-background-color has-background"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center","style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"}}}} -->
<div class="wp-block-column is-vertically-aligned-center" style="padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:group {"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:heading {"level":3,"textColor":"secondary","fontSize":"slider-title","fontFamily":"poppins"} -->
<h3 class="wp-block-heading has-secondary-color has-text-color has-poppins-font-family has-slider-title-font-size">Get our emails for info on new items, sales and more.</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"secondary","fontFamily":"poppins"} -->
<p class="has-secondary-color has-text-color has-poppins-font-family">We'll email you a voucher worth £10 off your first order over £50.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:shortcode -->
[contact-form-7 id="278" title="Untitled"]
<!-- /wp:shortcode --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center","style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"}}}} -->
<div class="wp-block-column is-vertically-aligned-center" style="padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:group {"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:heading {"level":3,"textColor":"secondary","fontSize":"slider-title","fontFamily":"poppins"} -->
<h3 class="wp-block-heading has-secondary-color has-text-color has-poppins-font-family has-slider-title-font-size">Need help?<br>(+800) 1234 5678 90</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"secondary","fontFamily":"poppins"} -->
<p class="has-secondary-color has-text-color has-poppins-font-family">We are available 8:00am – 7:00pm</p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:columns {"style":{"spacing":{"blockGap":{"top":"20px","left":"20px"}}}} -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":444,"width":128,"height":38,"sizeSlug":"full","linkDestination":"none","style":{"border":{"width":"2px","radius":"8px"}},"borderColor":"background"} -->
<figure class="wp-block-image size-full is-resized has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/footer-img-01-1.png" alt="" class="has-border-color has-background-border-color wp-image-444" style="border-width:2px;border-radius:8px" width="128" height="38"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":286,"sizeSlug":"full","linkDestination":"none","style":{"border":{"width":"2px","radius":"8px"}},"borderColor":"background"} -->
<figure class="wp-block-image size-full has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/footer-img-02.png" alt="" class="has-border-color has-background-border-color wp-image-286" style="border-width:2px;border-radius:8px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"fontFamily":"poppins"} -->
<p class="has-poppins-font-family"><strong>Shopping App:</strong>&nbsp;Try our View in Your Room feature, manage registries and save payment info.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"2rem","bottom":"2rem"},"blockGap":"0px"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"backgroundColor":"background","textColor":"body-text","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull has-body-text-color has-background-background-color has-text-color has-background has-link-color" style="padding-top:2rem;padding-bottom:2rem"><!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"bottom":"3rem","top":"3rem"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:3rem;padding-bottom:3rem"><!-- wp:column {"width":"28%","style":{"spacing":{"padding":{"top":"20px","right":"20px","bottom":"20px","left":"0px"},"blockGap":"10px"}},"className":" animated animated-fadeInUp"} -->
<div class="wp-block-column animated animated-fadeInUp" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:0px;flex-basis:28%"><!-- wp:group {"style":{"spacing":{"blockGap":"6px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp has-body-text-color has-text-color has-link-color"><!-- wp:heading {"level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500","fontSize":"1.5rem"}},"textColor":"body-text","fontFamily":"poppins"} -->
<h3 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family" style="font-size:1.5rem;font-style:normal;font-weight:500">Mart</h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-medium-font-size">Pulvinar elementum integer enim neque volutpat ac tincidunt. Id faucibus nisl tincidunt eget nullam.Id faucibus nisl tincidunt eget </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"small","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-small-font-size">(+800) 1234 5678 90 – info@example.com</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"","style":{"spacing":{"blockGap":"10px","padding":{"top":"20px"}}}} -->
<div class="wp-block-column" style="padding-top:20px"><!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"textColor":"body-text","className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp has-body-text-color has-text-color"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":"1.5rem","fontStyle":"normal","fontWeight":"500","textTransform":"capitalize"}},"textColor":"body-text","fontFamily":"poppins"} -->
<h3 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family" style="font-size:1.5rem;font-style:normal;font-weight:500;text-transform:capitalize">Information</h3>
<!-- /wp:heading -->

<!-- wp:list {"fontSize":"medium","fontFamily":"poppins"} -->
<ul class="has-poppins-font-family has-medium-font-size"><!-- wp:list-item -->
<li>About Us</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Privacy Policy</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Returns Policy</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Shipping Policy</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Dropshipping</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"","style":{"spacing":{"padding":{"top":"20px"},"blockGap":"10px"}}} -->
<div class="wp-block-column" style="padding-top:20px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:heading {"level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500","fontSize":"1.5rem"}},"textColor":"body-text","fontFamily":"poppins"} -->
<h3 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family" style="font-size:1.5rem;font-style:normal;font-weight:500">Categories</h3>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:list {"textColor":"body-text","fontSize":"medium","fontFamily":"poppins"} -->
<ul class="has-body-text-color has-text-color has-poppins-font-family has-medium-font-size"><!-- wp:list-item -->
<li>Women</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Men</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Bags</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Outerwear</li>
<!-- /wp:list-item -->

<!-- wp:list-item -->
<li>Shoes</li>
<!-- /wp:list-item --></ul>
<!-- /wp:list --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"","style":{"spacing":{"padding":{"top":"20px"},"blockGap":"10px"}}} -->
<div class="wp-block-column" style="padding-top:20px"><!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"textColor":"body-text","className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp has-body-text-color has-text-color"><!-- wp:heading {"level":3,"style":{"typography":{"fontStyle":"normal","fontWeight":"500","fontSize":"1.5rem","textTransform":"capitalize"}},"textColor":"body-text","fontFamily":"poppins"} -->
<h3 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family" style="font-size:1.5rem;font-style:normal;font-weight:500;text-transform:capitalize">Contact Details</h3>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"20px"},"typography":{"lineHeight":"1.9"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="line-height:1.9"><!-- wp:paragraph {"fontFamily":"poppins"} -->
<p class="has-poppins-font-family">Feel free to contact &amp; reach us !</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontFamily":"poppins"} -->
<p class="has-poppins-font-family">Address : 22th Streets, Colorado<br>Email :&nbsp;info@domain.com<br>Phone : +988-256-266-88</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"15px","bottom":"15px"}},"elements":{"link":{"color":{"text":"var:preset|color|background"}}}},"textColor":"background","className":" animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignfull animated animated-fadeInUp has-background-color has-text-color has-link-color" style="padding-top:15px;padding-bottom:15px"><!-- wp:separator {"align":"wide","className":"is-style-wide"} -->
<hr class="wp-block-separator alignwide has-alpha-channel-opacity is-style-wide"/>
<!-- /wp:separator -->

<!-- wp:group {"align":"wide","layout":{"type":"flex","allowOrientation":false,"justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"align":"wide","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group alignwide"><!-- wp:paragraph {"align":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"textColor":"body-text","fontFamily":"poppins"} -->
<p class="has-text-align-center has-body-text-color has-text-color has-link-color has-poppins-font-family"> Copyright  © 2023 mart -  WordPress Theme : by <a href="https://sparklewpthemes.com/" rel="designer noopener" target="_blank">Sparkle Themes</a>  </p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":324,"width":296,"height":20,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/cards.png" alt="" class="wp-image-324" width="296" height="20"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
