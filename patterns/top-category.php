<?php
 /**
  * Title: Top-Category
  * Slug: woocommerce-fse/top-category
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"30px","right":"20px","bottom":"30px","left":"20px"},"blockGap":"0px"}},"backgroundColor":"background","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide has-background-background-color has-background" style="padding-top:30px;padding-right:20px;padding-bottom:30px;padding-left:20px"><!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"50px","bottom":"50px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="padding-top:50px;padding-bottom:50px"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"30%"} -->
<div class="wp-block-column" style="flex-basis:30%"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"http://localhost:10053/wp-content/uploads/2023/04/banner-box4.jpg","id":242,"dimRatio":0,"focalPoint":{"x":1,"y":0.45},"minHeight":860,"minHeightUnit":"px","contentPosition":"bottom left","className":"image-zoom-hover","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left image-zoom-hover" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:860px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-242" alt="" src="http://localhost:10053/wp-content/uploads/2023/04/banner-box4.jpg" style="object-position:100% 45%" data-object-fit="cover" data-object-position="100% 45%"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px","padding":{"bottom":"100px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-bottom:100px"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:500">10% Off All Items</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0","padding":{"top":"0px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-top:0px"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-left has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Decorate your life With Arts</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">See Collection </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"70%"} -->
<div class="wp-block-column" style="flex-basis:70%"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px"}},"textColor":"body-text","className":" animated animated-fadeInUp","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-body-text-color has-text-color"><!-- wp:paragraph {"align":"left","style":{"typography":{"letterSpacing":"1px","fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","className":"sp-underline","fontSize":"content-heading","fontFamily":"poppins"} -->
<p class="has-text-align-left sp-underline has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500;letter-spacing:1px"><strong>Top Categories</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"500","textTransform":"uppercase"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"className":"has-minus-margin","fontSize":"small","fontFamily":"poppins"} -->
<p class="has-minus-margin has-link-color has-poppins-font-family has-small-font-size" style="font-style:normal;font-weight:500;text-transform:uppercase"><span class="elementor-button-content-wrapper" style="box-sizing: border-box; display: inline !important; justify-content: center; text-decoration: inherit;"><span class="elementor-button-text" style="box-sizing: border-box; flex-grow: 1; order: 10; display: inline !important; text-decoration: inherit;"><a href="#">View More →</a></span></span></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:query {"queryId":46,"query":{"perPage":"4","pages":0,"offset":0,"postType":"product","order":"asc","orderBy":"date","author":"","search":"","exclude":[],"sticky":"","inherit":false,"taxQuery":{"product_cat":[26,24,30,23]},"parents":[]},"displayLayout":{"type":"flex","columns":4},"align":"wide"} -->
<div class="wp-block-query alignwide"><!-- wp:post-template {"textColor":"foreground"} -->
<!-- wp:post-featured-image {"isLink":true,"width":"200px","height":"200px"} /-->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"10px","right":"10px","bottom":"10px","left":"10px"},"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-body-text-color has-text-color has-link-color" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px"><!-- wp:post-title {"textAlign":"center","level":6,"isLink":true,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"has-one-line","fontSize":"medium","fontFamily":"poppins"} /-->

<!-- wp:woocommerce/product-price {"isDescendentOfQueryLoop":true,"textAlign":"center"} /-->

<!-- wp:group {"className":"hide-background","layout":{"type":"constrained"}} -->
<div class="wp-block-group hide-background"><!-- wp:woocommerce/product-button {"isDescendentOfQueryLoop":true} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
<!-- /wp:post-template --></div>
<!-- /wp:query --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:query {"queryId":46,"query":{"perPage":"4","pages":0,"offset":0,"postType":"product","order":"desc","orderBy":"title","author":"","search":"","exclude":[],"sticky":"","inherit":false,"taxQuery":{"product_cat":[23,24,26,31]},"parents":[]},"displayLayout":{"type":"flex","columns":4},"align":"wide"} -->
<div class="wp-block-query alignwide"><!-- wp:post-template {"style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text"} -->
<!-- wp:post-featured-image {"isLink":true,"width":"200px","height":"200px"} /-->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"10px","right":"10px","bottom":"10px","left":"10px"},"blockGap":"10px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px"><!-- wp:post-title {"textAlign":"center","level":6,"isLink":true,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"className":"has-one-line","fontSize":"medium","fontFamily":"poppins"} /-->

<!-- wp:woocommerce/product-price {"isDescendentOfQueryLoop":true,"textAlign":"center"} /-->

<!-- wp:group {"className":"hide-background","layout":{"type":"constrained"}} -->
<div class="wp-block-group hide-background"><!-- wp:woocommerce/product-button {"isDescendentOfQueryLoop":true} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
<!-- /wp:post-template --></div>
<!-- /wp:query --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
