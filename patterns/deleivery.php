<?php
 /**
  * Title:Deleivery
  * Slug: woocommerce-fse/deleivery
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"29px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="padding-top:29px;padding-bottom:30px"><!-- wp:columns {"align":"wide","backgroundColor":"border"} -->
<div class="wp-block-columns alignwide has-border-background-color has-background"><!-- wp:column {"style":{"border":{"right":{"color":"#cccccc","width":"1px"}}}} -->
<div class="wp-block-column" style="border-right-color:#cccccc;border-right-width:1px"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"20px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":4087,"width":45,"height":45,"sizeSlug":"full","linkDestination":"none","style":{"color":{"duotone":["#e24e62","#e24e62"]}}} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Vector-1.png" alt="" class="wp-image-4087" width="45" height="45"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-body-text-color has-text-color has-link-color"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-body-text-color has-text-color has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500">FREE DELIVERY</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"primary","fontFamily":"poppins"} -->
<p class="has-primary-color has-text-color has-poppins-font-family">From $59.89</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"border":{"right":{"color":"#cccccccc","width":"1px"}}}} -->
<div class="wp-block-column" style="border-right-color:#cccccccc;border-right-width:1px"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"20px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":4092,"width":45,"height":45,"sizeSlug":"full","linkDestination":"none","style":{"color":{"duotone":["#e24e62","#e24e62"]}}} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Vector-2.png" alt="" class="wp-image-4092" width="45" height="45"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500">SUPPORT 24/7</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"primary","fontFamily":"poppins"} -->
<p class="has-primary-color has-text-color has-poppins-font-family">Online 24 hours</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"border":{"right":{"color":"#cccccc","width":"1px"}}}} -->
<div class="wp-block-column" style="border-right-color:#cccccc;border-right-width:1px"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"20px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":4097,"width":45,"height":45,"sizeSlug":"full","linkDestination":"none","style":{"color":{"duotone":["#e24e62","#e24e62"]}}} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Vector-3.png" alt="" class="wp-image-4097" width="45" height="45"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500">FREE RETURN</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"primary","fontFamily":"poppins"} -->
<p class="has-primary-color has-text-color has-poppins-font-family">365 a days</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"20px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":4100,"width":45,"height":45,"sizeSlug":"full","linkDestination":"none","style":{"color":{"duotone":["#e24e62","#e24e62"]}}} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/10.-Growth.png" alt="" class="wp-image-4100" width="45" height="45"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500">PAYMENT METHOD</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"textColor":"primary","fontFamily":"poppins"} -->
<p class="has-primary-color has-text-color has-poppins-font-family">From $59.89</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
