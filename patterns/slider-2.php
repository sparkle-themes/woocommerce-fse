<?php
 /**
  * Title:Slider #2
  * Slug: woocommerce-fse/slider-2
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"20px","right":"20px","bottom":"20px","left":"20px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull" style="padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"radius":"10px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="border-radius:10px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-34-1-1024x587-1.png","id":681,"dimRatio":20,"minHeight":411,"minHeightUnit":"px"} -->
<div class="wp-block-cover" style="min-height:411px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-681" alt="" src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-34-1-1024x587-1.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"40%"} -->
<div class="wp-block-column" style="flex-basis:40%"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"radius":"15px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="border-radius:15px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-3.png","id":4048,"dimRatio":20,"focalPoint":{"x":1,"y":0.48},"minHeight":190,"minHeightUnit":"px"} -->
<div class="wp-block-cover" style="min-height:190px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-4048" alt="" src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-3.png" style="object-position:100% 48%" data-object-fit="cover" data-object-position="100% 48%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-medium-font-size">55% OFF !</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-poppins-font-family has-large-font-size">ELECTRONICS</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"secondary","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">Shop Now </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"radius":"15px"}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color" style="border-radius:15px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-6.png","id":4078,"dimRatio":20,"focalPoint":{"x":0.08,"y":0.87},"minHeight":190,"minHeightUnit":"px","isDark":false} -->
<div class="wp-block-cover is-light" style="min-height:190px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-4078" alt="" src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-6.png" style="object-position:8% 87%" data-object-fit="cover" data-object-position="8% 87%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"align":"right","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-right has-poppins-font-family has-medium-font-size">55% OFF !</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"right","style":{"typography":{"textTransform":"uppercase"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-right has-poppins-font-family has-large-font-size" style="text-transform:uppercase">couch</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"secondary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">Shop Now </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"border":{"radius":"15px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="border-radius:15px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-5.png","id":4075,"dimRatio":20,"focalPoint":{"x":0.02,"y":0.8},"minHeight":190,"minHeightUnit":"px","isDark":false} -->
<div class="wp-block-cover is-light" style="min-height:190px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-4075" alt="" src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-5.png" style="object-position:2% 80%" data-object-fit="cover" data-object-position="2% 80%"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"align":"right","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-right has-poppins-font-family has-medium-font-size">55% OFF !</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"textAlign":"right","fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-right has-poppins-font-family has-large-font-size"> WATCH</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"secondary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">Shop Now </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"border":{"radius":"15px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="border-radius:15px"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-7.png","id":4084,"dimRatio":20,"minHeight":190,"minHeightUnit":"px","isDark":false} -->
<div class="wp-block-cover is-light" style="min-height:190px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-20 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-4084" alt="" src="https://demo.sparkletheme.com/sparkle-fse/furniture/wp-content/uploads/sites/38/2023/04/Group-7.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"spacing":{"blockGap":"0px"}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:heading {"textAlign":"left","fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-left has-poppins-font-family has-large-font-size">Shoes</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"left","style":{"typography":{"textTransform":"uppercase"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-left has-poppins-font-family has-medium-font-size" style="text-transform:uppercase">upto 25% OFF !</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"textColor":"secondary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"left"}} -->
<div class="wp-block-group has-secondary-color has-text-color"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">Shop Now </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->