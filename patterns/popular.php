<?php
 /**
  * Title:Popular
  * Slug: woocommerce-fse/popular
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"style":{"spacing":{"blockGap":"10px","padding":{"top":"30px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:30px;padding-bottom:30px"><!-- wp:group {"align":"wide","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"align":"wide","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px"}},"className":" animated animated-fadeInUp","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp"><!-- wp:paragraph {"align":"left","style":{"typography":{"letterSpacing":"1px","fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","className":"sp-underline","fontSize":"content-heading","fontFamily":"poppins"} -->
<p class="has-text-align-left sp-underline has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500;letter-spacing:1px"><strong>Most Popular Product</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"500","textTransform":"uppercase"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"textColor":"body-text","className":"has-minus-margin","fontSize":"small","fontFamily":"poppins"} -->
<p class="has-minus-margin has-body-text-color has-text-color has-link-color has-poppins-font-family has-small-font-size" style="font-style:normal;font-weight:500;text-transform:uppercase"><span class="elementor-button-content-wrapper" style="box-sizing: border-box; display: inline !important; justify-content: center; text-decoration: inherit;"><span class="elementor-button-text" style="box-sizing: border-box; flex-grow: 1; order: 10; display: inline !important; text-decoration: inherit;"><a href="#">View More →</a></span></span></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"100px","bottom":"100px","right":"20px","left":"20px"},"blockGap":"50px"},"border":{"width":"0px","style":"none"}},"backgroundColor":"background","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide has-background-background-color has-background" style="border-style:none;border-width:0px;padding-top:100px;padding-right:20px;padding-bottom:100px;padding-left:20px"><!-- wp:query {"queryId":11,"query":{"perPage":"6","pages":0,"offset":0,"postType":"product","order":"asc","orderBy":"title","author":"","search":"","exclude":[],"sticky":"","inherit":false,"parents":[],"taxQuery":{"product_cat":[25,24,23,31]}},"displayLayout":{"type":"flex","columns":6},"align":"wide"} -->
<div class="wp-block-query alignwide"><!-- wp:post-template {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"primary","className":"has-position","fontSize":"medium","fontFamily":"poppins"} -->
<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:post-featured-image {"isLink":true,"width":"200px","height":"200px"} /--></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"15px"},"blockGap":"5px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:15px"><!-- wp:post-title {"textAlign":"center","isLink":true,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"has-one-line","fontSize":"content-heading","fontFamily":"poppins"} /-->

<!-- wp:woocommerce/product-price {"isDescendentOfQueryLoop":true,"textAlign":"center"} /-->

<!-- wp:woocommerce/product-sale-badge {"isDescendentOfQueryLoop":true,"className":"sale-has-position sale-color"} /-->

<!-- wp:group {"className":"hide-background","layout":{"type":"constrained"}} -->
<div class="wp-block-group hide-background"><!-- wp:woocommerce/product-button {"isDescendentOfQueryLoop":true,"className":"hide-background"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
<!-- /wp:post-template --></div>
<!-- /wp:query -->

<!-- wp:query {"queryId":11,"query":{"perPage":"6","pages":0,"offset":0,"postType":"product","order":"desc","orderBy":"title","author":"","search":"","exclude":[],"sticky":"","inherit":false,"parents":[],"taxQuery":{"product_cat":[24,30,26,27]}},"displayLayout":{"type":"flex","columns":6},"align":"wide"} -->
<div class="wp-block-query alignwide"><!-- wp:post-template {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"textColor":"primary","className":"has-position","fontSize":"medium","fontFamily":"poppins"} -->
<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:post-featured-image {"isLink":true,"width":"200px","height":"200px"} /--></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"padding":{"top":"15px"},"blockGap":"5px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:15px"><!-- wp:post-title {"textAlign":"center","isLink":true,"style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"has-one-line","fontSize":"content-heading","fontFamily":"poppins"} /-->

<!-- wp:woocommerce/product-price {"isDescendentOfQueryLoop":true,"textAlign":"center"} /-->

<!-- wp:woocommerce/product-sale-badge {"isDescendentOfQueryLoop":true,"className":"sale-has-position sale-color"} /-->

<!-- wp:group {"className":"hide-background","layout":{"type":"constrained"}} -->
<div class="wp-block-group hide-background"><!-- wp:woocommerce/product-button {"isDescendentOfQueryLoop":true,"className":"hide-background"} /--></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
<!-- /wp:post-template --></div>
<!-- /wp:query --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->
