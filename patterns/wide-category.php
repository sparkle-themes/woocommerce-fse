<?php
 /**
  * Title: Wide-Category
  * Slug: woocommerce-fse/wide-category
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"30px","bottom":"50px"}}},"className":"animated animated-fadeInUp","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp" style="padding-top:30px;padding-bottom:50px"><!-- wp:columns {"align":"wide","style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"}}}} -->
<div class="wp-block-columns alignwide" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:column {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-column" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-48-1.png","id":206,"dimRatio":40,"focalPoint":{"x":0.75,"y":0.56},"minHeight":257,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgb(246,42,76) 3%,rgb(244,93,103) 91%)","contentPosition":"bottom left","isDark":false,"className":"image-zoom-hover","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover is-light has-custom-content-position is-position-bottom-left image-zoom-hover" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:257px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-40 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgb(246,42,76) 3%,rgb(244,93,103) 91%)"></span><img class="wp-block-cover__image-background wp-image-206" alt="" src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-48-1.png" style="object-position:75% 56%" data-object-fit="cover" data-object-position="75% 56%"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px","padding":{"bottom":"100px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-bottom:100px"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:500">10% Off All Items</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0","padding":{"top":"0px"}}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color" style="padding-top:0px"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-left has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Decorate your life With Arts</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">See Collection </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-column" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"textColor":"secondary","className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp has-secondary-color has-text-color"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-48-2.png","id":214,"dimRatio":10,"focalPoint":{"x":0.22,"y":0.47},"minHeight":257,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)","contentPosition":"bottom left","isDark":false,"className":"image-zoom-hover","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover is-light has-custom-content-position is-position-bottom-left image-zoom-hover" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:257px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-10 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)"></span><img class="wp-block-cover__image-background wp-image-214" alt="" src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-48-2.png" style="object-position:22% 47%" data-object-fit="cover" data-object-position="22% 47%"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px","padding":{"bottom":"100px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-bottom:100px"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:500"> Sale 20% Off All Items</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0","padding":{"top":"0px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0px"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-left has-link-color has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:600">XBox One + S Controller</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">See Collection </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"style":{"spacing":{"padding":{"top":"0px","right":"0px","bottom":"0px","left":"0px"},"blockGap":"0px"}}} -->
<div class="wp-block-column" style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-48-3.png","id":218,"dimRatio":40,"minHeight":257,"minHeightUnit":"px","customGradient":"linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)","contentPosition":"bottom left","isDark":false,"className":"image-zoom-hover","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover is-light has-custom-content-position is-position-bottom-left image-zoom-hover" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:257px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-40 has-background-dim wp-block-cover__gradient-background has-background-gradient" style="background:linear-gradient(180deg,rgba(0,0,0,0.13) 3%,rgba(9,9,9,0.92) 91%)"></span><img class="wp-block-cover__image-background wp-image-218" alt="" src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-48-3.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px","padding":{"bottom":"100px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-bottom:100px"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:500">10% Off All Items</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0","padding":{"top":"0px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:0px"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-left has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Decorate your life With Arts</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">See Collection </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->