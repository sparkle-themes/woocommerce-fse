<?php
 /**
  * Title:Advertise-Banner
  * Slug: woocommerce-fse/advertise-banner
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","bottom":"30px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group" style="padding-top:30px;padding-bottom:30px"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"70%"} -->
<div class="wp-block-column" style="flex-basis:70%"><!-- wp:cover {"url":"http://localhost:10053/wp-content/uploads/2023/04/image-50.png","id":272,"dimRatio":0,"focalPoint":{"x":0.5,"y":0.18},"minHeight":204,"minHeightUnit":"px","isDark":false} -->
<div class="wp-block-cover is-light" style="min-height:204px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-272" alt="" src="http://localhost:10053/wp-content/uploads/2023/04/image-50.png" style="object-position:50% 18%" data-object-fit="cover" data-object-position="50% 18%"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:cover {"url":"http://localhost:10053/wp-content/uploads/2023/04/image-50-1-1.png","id":276,"dimRatio":0,"minHeight":204,"minHeightUnit":"px"} -->
<div class="wp-block-cover" style="min-height:204px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-276" alt="" src="http://localhost:10053/wp-content/uploads/2023/04/image-50-1-1.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
