<?php
 /**
  * Title: Popular #2
  * Slug: woocommerce-fse/popular-2
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"full","style":{"spacing":{"blockGap":"50px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"textColor":"body-text","className":" animated animated-fadeInUp","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-body-text-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","style":{"typography":{"letterSpacing":"1px","fontStyle":"normal","fontWeight":"500"}},"className":"sp-underline","fontSize":"content-heading","fontFamily":"poppins"} -->
<p class="has-text-align-left sp-underline has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500;letter-spacing:1px"><strong>Most Popular Product</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"500","textTransform":"uppercase"},"elements":{"link":{"color":{"text":"var:preset|color|foreground"}}}},"textColor":"body-text","className":"has-minus-margin","fontSize":"small","fontFamily":"poppins"} -->
<p class="has-minus-margin has-body-text-color has-text-color has-link-color has-poppins-font-family has-small-font-size" style="font-style:normal;font-weight:500;text-transform:uppercase"><span class="elementor-button-content-wrapper" style="box-sizing: border-box; display: inline !important; justify-content: center; text-decoration: inherit;"><span class="elementor-button-text" style="box-sizing: border-box; flex-grow: 1; order: 10; display: inline !important; text-decoration: inherit;"><a href="#">View More →</a></span></span></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":134,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Group-10.png" alt="" class="wp-image-134" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-secondary-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/alexa/">Alexa</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-text-align-center has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><s>$190.00&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","textDecoration":"underline"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600;text-decoration:underline">$160.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-secondary-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":583,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Bag-11-300x300-removebg-preview.png" alt="" class="wp-image-583" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-secondary-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/black-lady-purse/">Black Lady Purse</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><del>$</del>9<del>0.00</del>&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}},"fontSize":"medium"} -->
<p class="has-medium-font-size" style="text-decoration:underline">$70.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":568,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/product_11-removebg-preview.png" alt="" class="wp-image-568" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"body-text","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-body-text-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/casio-watch/">Casio Watch</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><s>$200.00</s></s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$150.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-secondary-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":86,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-40-2.png" alt="" class="wp-image-86" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/converse/">Converse</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"spacing":{"blockGap":"8px"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph -->
<p><s>$50.00</s>  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$30.00  </p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-body-text-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":132,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Group-9.png" alt="" class="wp-image-132" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/couch/">Couch</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">$80.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline">ADD TO CART</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-body-text-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":573,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/product_21-removebg-preview.png" alt="" class="wp-image-573" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/cream/">Cream</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><s>$50.00</s>&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$35.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-body-text-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":585,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Bag-18-300x300-removebg-preview.png" alt="" class="wp-image-585" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/yellow-travel-suitcase/">Yellow Travel Suit</a>...</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-text-align-center has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><s><del>$250.00</del>&nbsp;&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","textDecoration":"underline"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600;text-decoration:underline">$180.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-body-text-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":259,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/vr.jpg" alt="" class="wp-image-259" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/vr/">Vr</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><del>$</del><s>$250.00</s></s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}},"fontSize":"medium"} -->
<p class="has-medium-font-size" style="text-decoration:underline">$190.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-body-text-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":565,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/product_10-removebg-preview.png" alt="" class="wp-image-565" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/564/">Titan Watch</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><s>$150.00</s></s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$120.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"},"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative has-body-text-color has-text-color has-link-color"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":130,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Group-8.png" alt="" class="wp-image-130" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/teddy/">Teddy</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"spacing":{"blockGap":"8px"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph -->
<p><s>$50.00</s>  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$20.00  </p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":560,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/product_17-300x300-removebg-preview-1.png" alt="" class="wp-image-560" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-body-text-color has-text-color has-link-color has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/sweater/">Sweater</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"spacing":{"blockGap":"8px"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s>$60.00</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"}}} -->
<p class="has-text-align-center" style="text-decoration:underline">$40.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":257,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/chair.jpg" alt="" class="wp-image-257" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-body-text-color has-text-color has-link-color has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/study-chair/">study chair</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph -->
<p>$120.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","style":{"color":{"text":"#fffffe"}},"fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-text-color has-poppins-font-family has-medium-font-size" style="color:#fffffe">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->