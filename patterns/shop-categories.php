<?php
 /**
  * Title: Shop-Categories
  * Slug: woocommerce-fse/shop-categories
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"align":"wide","style":{"border":{"width":"0px","style":"none"},"spacing":{"padding":{"top":"50px","bottom":"80px"}}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="border-style:none;border-width:0px;padding-top:50px;padding-bottom:80px"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"textColor":"body-text","layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group has-body-text-color has-text-color"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px"}},"className":" animated animated-fadeInUp","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp"><!-- wp:paragraph {"align":"left","style":{"typography":{"letterSpacing":"1px","fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","className":"sp-underline","fontSize":"content-heading","fontFamily":"poppins"} -->
<p class="has-text-align-left sp-underline has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500;letter-spacing:1px"><strong>SHOP BY CATEGORIES</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"textColor":"primary","className":"has-minus-margin","fontSize":"small","fontFamily":"poppins"} -->
<p class="has-minus-margin has-primary-color has-text-color has-link-color has-poppins-font-family has-small-font-size" style="font-style:normal;font-weight:500"><span class="elementor-button-content-wrapper" style="box-sizing: border-box; display: inline !important; justify-content: center; text-decoration: inherit;"><span class="elementor-button-text" style="box-sizing: border-box; flex-grow: 1; order: 10; display: inline !important; text-decoration: inherit;"><a href="#">CATEGORIES</a></span></span></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"blockGap":"0","padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"}},"border":{"radius":"8px"}},"backgroundColor":"background","className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark has-background-background-color has-background" style="border-radius:8px;padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:image {"align":"center","id":86,"width":124,"height":124,"sizeSlug":"full","linkDestination":"custom","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full is-resized has-custom-border"><a href="#"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-40-2.png" alt="" class="wp-image-86" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px" width="124" height="124"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"textColor":"body-text","fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-body-text-color has-text-color has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:600"><a href="#">Shoes</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"},"blockGap":"0"},"border":{"radius":"8px"}},"backgroundColor":"background","className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark has-background-background-color has-background" style="border-radius:8px;padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:image {"align":"center","id":825,"sizeSlug":"full","linkDestination":"custom","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/cat-1-300x300-1.jpg" alt="" class="wp-image-825" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:600"><a href="#">Wardrobe</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"},"blockGap":"0"},"border":{"radius":"8px"}},"backgroundColor":"background","className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark has-background-background-color has-background" style="border-radius:8px;padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:image {"align":"center","id":827,"sizeSlug":"full","linkDestination":"custom","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/laptop.jpg" alt="" class="wp-image-827" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:600"><a href="#">Electronics</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"},"blockGap":"0"},"border":{"radius":"8px"}},"backgroundColor":"background","className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark has-background-background-color has-background" style="border-radius:8px;padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:image {"align":"center","id":101,"width":125,"height":125,"sizeSlug":"full","linkDestination":"custom","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full is-resized has-custom-border"><a href="#"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image_41__2_-removebg-preview-1-1.png" alt="" class="wp-image-101" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px" width="125" height="125"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:600"><a href="#">Watch</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"},"blockGap":"0"},"border":{"radius":"8px"}},"backgroundColor":"background","className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark has-background-background-color has-background" style="border-radius:8px;padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:image {"align":"center","id":829,"sizeSlug":"full","linkDestination":"custom","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full has-custom-border"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-41-3.png" alt="" class="wp-image-829" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:600"><a href="#">Cosmetics</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"30px","right":"30px","bottom":"30px","left":"30px"},"blockGap":"0"},"border":{"radius":"8px"}},"backgroundColor":"background","className":"has-shadow-dark"} -->
<div class="wp-block-group has-shadow-dark has-background-background-color has-background" style="border-radius:8px;padding-top:30px;padding-right:30px;padding-bottom:30px;padding-left:30px"><!-- wp:image {"align":"center","id":94,"width":125,"height":125,"sizeSlug":"full","linkDestination":"custom","style":{"border":{"radius":{"topLeft":"5px","topRight":"5px","bottomLeft":"0px","bottomRight":"0px"}}}} -->
<figure class="wp-block-image aligncenter size-full is-resized has-custom-border"><a href="#"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/bag-281x188-1-removebg-preview-1.png" alt="" class="wp-image-94" style="border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-left-radius:0px;border-bottom-right-radius:0px" width="125" height="125"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"fontSize":"content-heading","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-link-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:600"><a href="#">Bags</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->