<?php
 /**
  * Title: Top-Category #2
  * Slug: woocommerce-fse/top-category-2
  * Categories: woocommerce-fse
  */
?>
<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"50px","bottom":"50px"}}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide" style="padding-top:50px;padding-bottom:50px"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column {"width":"30%"} -->
<div class="wp-block-column" style="flex-basis:30%"><!-- wp:group {"className":" animated animated-fadeInUp"} -->
<div class="wp-block-group animated animated-fadeInUp"><!-- wp:cover {"url":"http://localhost:10053/wp-content/uploads/2023/04/banner-box4.jpg","id":242,"dimRatio":0,"focalPoint":{"x":1,"y":0.45},"minHeight":728,"minHeightUnit":"px","contentPosition":"bottom left","className":"image-zoom-hover","style":{"spacing":{"padding":{"top":"40px","right":"40px","bottom":"40px","left":"40px"}}}} -->
<div class="wp-block-cover has-custom-content-position is-position-bottom-left image-zoom-hover" style="padding-top:40px;padding-right:40px;padding-bottom:40px;padding-left:40px;min-height:728px"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-0 has-background-dim"></span><img class="wp-block-cover__image-background wp-image-242" alt="" src="http://localhost:10053/wp-content/uploads/2023/04/banner-box4.jpg" style="object-position:100% 45%" data-object-fit="cover" data-object-position="100% 45%"/><div class="wp-block-cover__inner-container"><!-- wp:spacer {"height":"27px"} -->
<div style="height:27px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px","padding":{"bottom":"100px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-bottom:100px"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"500"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:500">10% Off All Items</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"0","padding":{"top":"0px"}},"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","layout":{"type":"constrained"}} -->
<div class="wp-block-group has-secondary-color has-text-color has-link-color" style="padding-top:0px"><!-- wp:heading {"textAlign":"left","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"fontSize":"large","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-left has-poppins-font-family has-large-font-size" style="font-style:normal;font-weight:600"><a href="#">Decorate your life With Arts</a></h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"fontSize":"normal","fontFamily":"poppins"} -->
<p class="has-poppins-font-family has-normal-font-size">See Collection </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"className":"dashicons dashicons-arrow-right-alt"} -->
<p class="dashicons dashicons-arrow-right-alt"></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"70%"} -->
<div class="wp-block-column" style="flex-basis:70%"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px","padding":{"bottom":"30px"}}},"layout":{"inherit":false}} -->
<div class="wp-block-group alignwide" style="padding-bottom:30px"><!-- wp:group {"style":{"spacing":{"blockGap":"15px"}},"layout":{"inherit":true,"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:group {"align":"wide","style":{"spacing":{"blockGap":"10px"}},"textColor":"body-text","className":" animated animated-fadeInUp","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide animated animated-fadeInUp has-body-text-color has-text-color"><!-- wp:paragraph {"align":"left","style":{"typography":{"letterSpacing":"1px","fontStyle":"normal","fontWeight":"500"}},"textColor":"body-text","className":"sp-underline","fontSize":"content-heading","fontFamily":"poppins"} -->
<p class="has-text-align-left sp-underline has-body-text-color has-text-color has-poppins-font-family has-content-heading-font-size" style="font-style:normal;font-weight:500;letter-spacing:1px"><strong>Top Categories</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"500","textTransform":"uppercase"},"elements":{"link":{"color":{"text":"var:preset|color|primary"}}}},"className":"has-minus-margin","fontSize":"small","fontFamily":"poppins"} -->
<p class="has-minus-margin has-link-color has-poppins-font-family has-small-font-size" style="font-style:normal;font-weight:500;text-transform:uppercase"><span class="elementor-button-content-wrapper" style="box-sizing: border-box; display: inline !important; justify-content: center; text-decoration: inherit;"><span class="elementor-button-text" style="box-sizing: border-box; flex-grow: 1; order: 10; display: inline !important; text-decoration: inherit;"><a href="#">View More →</a></span></span></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":134,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Group-10.png" alt="" class="wp-image-134" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-body-text-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/alexa/">Alexa</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-text-align-center has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><s>$190.00&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","textDecoration":"underline"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600;text-decoration:underline">$160.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","textColor":"background-secondary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-secondary-color has-text-color has-poppins-font-family has-medium-font-size">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":583,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Bag-11-300x300-removebg-preview.png" alt="" class="wp-image-583" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-body-text-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/black-lady-purse/">Black Lady Purse</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><del>$</del>9<del>0.00</del>&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}},"fontSize":"medium"} -->
<p class="has-medium-font-size" style="text-decoration:underline">$70.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","textColor":"background-secondary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-secondary-color has-text-color has-poppins-font-family has-medium-font-size">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":568,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/product_11-removebg-preview.png" alt="" class="wp-image-568" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-body-text-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/casio-watch/">Casio Watch</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><s>$200.00</s></s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$150.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","textColor":"background-secondary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-secondary-color has-text-color has-poppins-font-family has-medium-font-size">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":86,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/image-40-2.png" alt="" class="wp-image-86" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-body-text-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/converse/">Converse</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"spacing":{"blockGap":"8px"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph -->
<p><s>$50.00</s>  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$30.00  </p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:group {"align":"wide","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":585,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Bag-18-300x300-removebg-preview.png" alt="" class="wp-image-585" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|body-text"}}}},"textColor":"body-text","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-body-text-color has-text-color has-link-color has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/yellow-travel-suitcase/">Yellow Travel Suit</a>...</h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"}},"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-text-align-center has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><s><del>$250.00</del>&nbsp;&nbsp;</s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"fontStyle":"normal","fontWeight":"600","textDecoration":"underline"}},"textColor":"primary","fontSize":"medium"} -->
<p class="has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600;text-decoration:underline">$180.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","textColor":"background-secondary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-secondary-color has-text-color has-poppins-font-family has-medium-font-size">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":259,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/vr.jpg" alt="" class="wp-image-259" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/vr/">Vr</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><del>$</del><s>$250.00</s></s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}},"fontSize":"medium"} -->
<p class="has-medium-font-size" style="text-decoration:underline">$190.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","textColor":"background-secondary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-secondary-color has-text-color has-poppins-font-family has-medium-font-size">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":565,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/product_10-removebg-preview.png" alt="" class="wp-image-565" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/564/">Titan Watch</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"spacing":{"blockGap":"8px"},"typography":{"fontStyle":"normal","fontWeight":"600"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"},"fontSize":"medium"} -->
<div class="wp-block-group has-primary-color has-text-color has-medium-font-size" style="font-style:normal;font-weight:600"><!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center"><s><s>$150.00</s></s></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$120.00</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph -->

<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group"><!-- wp:group {"style":{"spacing":{"padding":{"top":"0px","bottom":"0px","left":"10px","right":"10px"}},"border":{"radius":"3px"}},"backgroundColor":"primary","className":"sale-has-position  s","layout":{"type":"constrained"}} -->
<div class="wp-block-group sale-has-position s has-primary-background-color has-background" style="border-radius:3px;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px"><!-- wp:paragraph {"align":"center","textColor":"background-secondary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-background-secondary-color has-text-color has-poppins-font-family has-medium-font-size">sale</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"spacing":{"blockGap":"10px"}},"className":"is-position-relative","layout":{"type":"constrained"}} -->
<div class="wp-block-group is-position-relative"><!-- wp:group {"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"align":"center","id":130,"width":150,"height":150,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image aligncenter size-full is-resized"><img src="https://demo.sparkletheme.com/sparkle-fse/mart/wp-content/uploads/sites/39/2023/04/Group-8.png" alt="" class="wp-image-130" width="150" height="150"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"full","layout":{"type":"constrained"},"fontFamily":"poppins"} -->
<div class="wp-block-group alignfull has-poppins-font-family"><!-- wp:heading {"textAlign":"center","fontSize":"medium","fontFamily":"poppins"} -->
<h2 class="wp-block-heading has-text-align-center has-poppins-font-family has-medium-font-size"><a href="https://demo.sparkletheme.com/sparkle-fse/mart/product/teddy/">Teddy</a></h2>
<!-- /wp:heading -->

<!-- wp:group {"style":{"typography":{"fontStyle":"normal","fontWeight":"600"},"spacing":{"blockGap":"8px"}},"textColor":"primary","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group has-primary-color has-text-color" style="font-style:normal;font-weight:600"><!-- wp:paragraph -->
<p><s>$50.00</s>  </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"typography":{"textDecoration":"underline"}}} -->
<p style="text-decoration:underline">$20.00  </p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:paragraph {"align":"center","style":{"typography":{"textDecoration":"underline"},"elements":{"link":{"color":{"text":"var:preset|color|tertiary"}}}},"textColor":"tertiary","fontSize":"medium","fontFamily":"poppins"} -->
<p class="has-text-align-center has-tertiary-color has-text-color has-link-color has-poppins-font-family has-medium-font-size" style="text-decoration:underline"><a href="#">ADD TO CART</a></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
