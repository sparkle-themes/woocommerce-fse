<?php
 /**
  * Title: Home Page
  * Slug: woocommerce-fse/home-page
  * Categories: woocommerce-fse
  */
?>
<!-- wp:pattern {"slug":"woocommerce-fse/slider"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/deleivery"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/shop-categories"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/new-arrival"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/wide-category"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/popular"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/advertise-banner"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/top-category"} /-->
<!-- wp:pattern {"slug":"woocommerce-fse/latest-news"} /-->
